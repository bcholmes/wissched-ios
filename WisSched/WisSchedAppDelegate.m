//
//  unslacked_appAppDelegate.m
//  unslacked-app
//
//  More comments
//  Created by BC Holmes on 11-07-01.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIColor-HexString/UIColor+HexString.h>

#import "WisSchedAppDelegate.h"
#import "BCHTheme.h"
#import "Appirater.h"

@interface WisSchedAppDelegate ()

@property (nonatomic) BOOL ratingsAfterDate;

@end

@implementation WisSchedAppDelegate

CLLocationDegrees defaultLat = 43.075613;
CLLocationDegrees defaultLon = -89.386584;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Appirater setAppId:@"524251160"];
    [Appirater setUsesUntilPrompt:-1];
    [Appirater setDaysUntilPrompt:-1];
    [Appirater setSignificantEventsUntilPrompt:1];
//    [Appirater setDebug:YES];
    
    self.currentCon = @"wiscon43";
    self.currentConName = @"Wiscon 43";
    self.timeZone = [NSTimeZone timeZoneWithName:@"America/Chicago"];
    self.contentJsonName = @"wisschedConInfo.json";
    self.theme = [[BCHTheme alloc] initWithBaseColor:
                  [UIColor colorWithRed:(64.0 / 255.0) green:(72.0 / 255.0) blue:(145.0 / 255.0) alpha:1]];
    self.theme.darkColor = [UIColor colorWithHexString:@"1A1942"];
    self.supportsLogin = YES;
    self.supportsRestaurants = YES;
    self.supportsConInfo = YES;
    self.supportsAnnouncements = YES;
    self.defaultLocation = [[CLLocation alloc] initWithLatitude:defaultLat longitude:defaultLon];
    // intelliware
    self.conventionLocation = [[CLLocation alloc] initWithLatitude:43.648688 longitude:-79.3892447];
    self.blogUrl = @"https://wiscon.net/wp-json/posts";
    BOOL result = [super application:application didFinishLaunchingWithOptions:launchOptions];
    [Appirater appLaunched:YES];
    return result;
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    [super applicationDidBecomeActive:application];
    
    if (!self.ratingsAfterDate) {
        NSDate* now = [NSDate date];

        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        
        dateFormatter.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        
        NSDate* lastDayStart = [dateFormatter dateFromString:@"2019-05-27T08:00:00-0500"];
        NSDate* lastDayEnd = [dateFormatter dateFromString:@"2019-05-27T23:59:59-0500"];
        if ([now timeIntervalSinceDate:lastDayStart] > 0 && [now timeIntervalSinceDate:lastDayEnd] < 0) {
            [Appirater userDidSignificantEvent:YES];
            self.ratingsAfterDate = YES;
        }
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [super applicationWillEnterForeground:application];
    [Appirater appEnteredForeground:YES];
}


@end
