//
//  main.m
//  WisSched
//
//  Created by BC Holmes on 2016-03-04.
//  Copyright © 2016 Ayizan Studios. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WisSchedAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WisSchedAppDelegate class]));
    }
}
